const { ApolloServer, gql } = require('apollo-server-micro');
const rebrickable = async url => {
  const fetch = require('node-fetch');
  const fullurl = `https://rebrickable.com${url}`;
  const headers = {
    authorization: `key ${process.env.REBRICKABLE_API_KEY}`,
    accept: 'application/json'
  };
  const res = await fetch(fullurl, { headers });
  const content = await res.json();
  console.log({
    req: { fullurl, headers },
    res: { headers: res.headers, content: content }
  });
  return content;
};

const typeDefs = gql`
  type Element {
    part_num: String
    name: String
    colorId: String
    sets: [Set]
  }

  type Set {
    set_num: String
    name: String
  }

  type Query {
    allSetsByElement(elementId: String): Element
  }
`;

const resolvers = {
  Query: {
    allSetsByElement: async (root, args, context) => {
      const res = await rebrickable(`/api/v3/lego/elements/${args.elementId}`);
      return {
        part_num: res.part.part_num,
        name: res.part.name,
        colorId: res.color.id
      };
    }
  },
  Element: {
    sets: async element => {
      const res = await rebrickable(
        `/api/v3/lego/parts/${element.part_num}/colors/${element.colorId}/sets`
      );
      return res.results.map(r => {
        return {
          set_num: r.set_num,
          name: r.name
        };
      });
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  introspection: true,
  playground: true
});

module.exports = server.createHandler({ path: '/api' });
